from typing import List, Tuple, Iterable, Optional
from django.db import transaction
from debiancontributors import parser, Identifier, Contribution
from collections import defaultdict
from contributors import models
import datetime
from .models import ImportStats
import gc

BatchSubmission = Iterable[Tuple[List[Identifier], List[Contribution]]]


def update_contribution_replace(c: models.Contribution, begin: datetime.date, until: datetime.date) -> bool:
    """
    Extend the begin-until date range with a new date range.

    If begin is missing, defaults to the previous begin value.
    If no begin value was ever set, default to today.

    If until is missing, default to today.

    Returns True if the date range was changed, else False.
    """
    changed = False

    # If missing, defaults to previous 'begin' value.
    if begin is None:
        begin = c.begin
    # If never seen, defaults to 'today'
    if begin is None:
        # If a data source only reports 'until', we have a better default
        # than 'today'
        if until is not None:
            begin = until
        else:
            begin = models._today()
    if c.begin != begin:
        c.begin = begin
        changed = True

    # If missing, defaults to 'today'
    if until is None:
        until = models._today()
    if c.until != until:
        c.until = until
        changed = True

    return changed


def update_contribution_extend(c: models.Contribution, begin: datetime.date, until: datetime.date) -> bool:
    """
    Extend the begin-until date range with a new date range.

    If begin is missing, defaults to the previous begin value.
    If no begin value was ever set, default to today.

    If until is missing, default to today.

    Returns True if the date range was changed, else False.
    """
    changed = False

    # If missing, defaults to previous 'begin' value.
    if begin is None:
        begin = c.begin
    # If never seen, defaults to 'today'
    if begin is None:
        # If a data source only reports 'until', we have a better default
        # than 'today'
        if until is not None:
            begin = until
        else:
            begin = models._today()
    if c.begin > begin:
        c.begin = begin
        changed = True

    # If missing, defaults to 'today'
    if until is None:
        until = models._today()
    if c.until < until:
        c.until = until
        changed = True

    return changed


class ContributionTypeCache:
    """
    Prefetched cache of contribution data by contribution type
    """
    def __init__(self, source: models.Source, name: str, results: ImportStats, update_contribution):
        self.source = source
        try:
            self.ctype = models.ContributionType.objects.get(source=source, name=name)
        except models.ContributionType.DoesNotExist:
            raise parser.Fail(404, "Source {} has no contribution type {}".format(source.name, name))
        self.results = results
        self.update_contribution = update_contribution

        # Preload contributions and identifiers
        self.contributions = {}
        self.identifiers = {}
        for c in models.Contribution.objects.filter(type=self.ctype).select_related("identifier"):
            i = c.identifier
            self.identifiers[(i.type, i.name)] = i
            self.contributions[i.pk] = c

        # If there are no contributions, we assume it's the first import, and
        # prefetch all identifiers to avoid looking them up one by one
        if not self.identifiers:
            self.identifiers = {
                (i.type, i.name): i
                for i in models.Identifier.objects.all()
            }

    def lookup_identifier(self, i: Identifier) -> models.Identifier:
        ident = self.identifiers.get((i.type, i.id))
        if ident is None:
            ident, _ = models.Identifier.objects.get_or_create(type=i.type, name=i.id)
            self.identifiers[(i.type, i.id)] = ident
        return ident

    # def get_contribution(self, ctype: str, ident: Identifier) -> Optional[models.Contribution]:
    #     return self.contributions.get((ctype, ident.type, ident.name))

    def import_contribution(self, ident: Identifier, contrib: Contribution) -> models.Contribution:
        """
        Validate and insert or update a contribution, extending its span if it
        already exists
        """
        ident = self.lookup_identifier(ident)
        cold = self.contributions.get(ident.pk)

        if cold is None:
            self.results.contributions_created += 1
            cnew = models.Contribution.objects.create(
                identifier=ident,
                type=self.ctype,
                begin=contrib.begin,
                until=contrib.end,
                url=contrib.url,
            )
            self.contributions[ident.pk] = cnew
            return cnew

        changed = self.update_contribution(cold, contrib.begin, contrib.end)

        if cold.url != contrib.url:
            cold.url = contrib.url
            changed = True

        if changed:
            self.results.contributions_updated += 1
            cold.save()

        return cold

    def recompute_aggregate(self):
        # Prefetch users
        hidden_users = frozenset(models.User.objects.filter(hidden=True).values_list("pk", flat=True))

        # Build a blacklist with the hidden user IDs for this source
        hidden_userpks = frozenset(models.UserSourceSettings.objects.filter(source=self.source, hidden=True)
                                   .values_list("user__pk", flat=True))

        # Group contributions by user ID
        by_userpk = {}
        for contribution in self.contributions.values():
            # Skip if identifier is hidden
            if contribution.identifier.hidden:
                continue

            # Get user pk
            user_pk = contribution.identifier.user_id

            # Skip if this source is hidden for this user
            if user_pk in hidden_userpks:
                continue

            # Skip if identifier is not associated
            if user_pk is None:
                continue

            # Skip if user is hidden
            if user_pk in hidden_users:
                continue

            # Aggregate by user
            old = by_userpk.get(user_pk, None)
            if old is None:
                by_userpk[user_pk] = (contribution.begin, contribution.until)
            else:
                by_userpk[user_pk] = (min(contribution.begin, old[0]), max(contribution.until, old[1]))

        # Replace agrgegation
        with transaction.atomic():
            # Delete old records
            models.AggregatedPersonContribution.objects.filter(ctype=self.ctype).delete()

            # Insert the new records
            for user_pk, (begin, until) in by_userpk.items():
                models.AggregatedPersonContribution.objects.create(
                        user_id=user_pk, ctype=self.ctype, begin=begin, until=until)


class BatchImport:
    def __init__(self, source: models.Source, results: ImportStats, batch: BatchSubmission):
        self.source = source
        self.results = results
        self.batch = batch
        self.latest_entry = None  # type: datetime.date

    @classmethod
    def by_name(cls, method: Optional[str]) -> "BatchImport":
        if not method or method == "replace":
            return ReplaceBatchImport
        elif method == "extend":
            return ExtendBatchImport
        else:
            raise parser.Fail(400, "import method {} is not one of (replace, extend)".format(method))

    def run(self):
        # Group contributions by contribution type
        by_ctype = defaultdict(list)
        try:
            for ids, contribs in self.batch:
                self.results.records_parsed += 1
                for c in contribs:
                    type_contribs = by_ctype[c.type]
                    for i in ids:
                        type_contribs.append((i, c))
        except parser.Fail as e:
            if not by_ctype:
                raise
            errors = getattr(e, "errors", None)
            if errors is not None:
                self.results.errors.extend(errors)
                self.results.records_skipped = len(errors)
            else:
                self.results.errors.append(e.msg)

        # Process one contribution type at a time
        for ctype, submissions in by_ctype.items():
            cache = ContributionTypeCache(self.source, ctype, self.results, self.update_contribution)
            with transaction.atomic():
                for i, c in submissions:
                    try:
                        # Track the highest contribution time
                        contrib = cache.import_contribution(i, c)
                        if self.latest_entry is None:
                            self.latest_entry = contrib.until
                        elif contrib.until is not None and contrib.until > self.latest_entry:
                            self.latest_entry = contrib.until
                        self.results.contributions_processed += 1
                    except parser.Fail as f:
                        self.results.record_fail(f)
                        self.results.contributions_skipped += 1
            cache.recompute_aggregate()
            cache = None
            gc.collect()


class ReplaceBatchImport(BatchImport):
    NAME = "replace"

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.update_contribution = update_contribution_replace


class ExtendBatchImport(BatchImport):
    NAME = "extend"

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.update_contribution = update_contribution_extend
