from importer import serializers
import contributors.models as cmodels
from signon.models import Identity

def export_public_data():
    users = serializers.UserSerializer(cmodels.User.objects.filter(hidden=False),
            many=True).data

    identifiers = serializers.IdentifierSerializer(
            cmodels.Identifier.objects.filter(hidden=False, user__hidden=False),
            many=True).data
    for each in identifiers:
        if each["type"] == "email":
            each["name"] = f"{each['id']}@contributors.debian.org"

    identities = serializers.IdentitySerializer(
            Identity.objects.filter(person__hidden=False), many=True).data

    sources = cmodels.Source.export_json()

    hidden_usersourcesettings = set(cmodels.UserSourceSettings.objects.filter(
            hidden=True).values_list("user__id", "source__id"))

    conts = cmodels.Contribution.objects.filter(identifier__hidden=False,
            identifier__user__hidden=False)
    conts = [c for c in conts if (c.identifier.user_id, c.type.source_id) not in hidden_usersourcesettings]
    contributions = serializers.ContributionSerializer(conts, many=True).data

    fres = {
            "version": 1,
            "users": users,
            "identifiers": identifiers,
            "identities": identities,
            "sources": sources,
            "contributions": contributions
            }
    return fres
