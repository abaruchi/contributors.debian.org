# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from debiancontributors import parser
from contributors import models as bmodels
from importer import importer
from importer.models import Submission
import getpass
import sys
import logging
import json
import os
import gzip
import shutil
import tempfile

# log = logging.getLogger(__name__)


def import_file(source, method, fname):
    res = Submission()
    res.received = timezone.now().astimezone(timezone.utc)
    res.source = source

    with res.track("receive") as (log, stats):
        # Log request metadata
        stats.request = {
            "source": "command line",
            "user": getpass.getuser(),
        }

        # Get batch import method name
        if method not in ("replace", "extend"):
            raise CommandError(f"import method {method} is not one of (replace, extend)")
        res.method = method

        # Save submission content
        # Uncompress and json-decode the submission data from a POST request

        # Detect compression type guessing it on uploaded file name
        file_ext = os.path.splitext(fname)[1]
        compression = file_ext.replace(os.path.extsep, '') or True
        if compression == "json":
            compression = ''

        # Decode the data
        with open(fname, "rb") as fd:
            parsed = parser.get_json(fd, compression)

        # Validate the submission contents
        dcparser = parser.Parser()
        records_parsed = 0
        try:
            for _ in dcparser.parse_submission(parsed):
                records_parsed += 1
        except parser.Fail as e:
            errors = getattr(e, "errors", None)
            if errors is not None:
                stats.errors.extend(errors)
            else:
                stats.errors.append(e.msg)
        stats.records_parsed = records_parsed

        # Save the submission
        res.relpath = "{:%Y%m%d-%H:%M:%S}.json.gz".format(res.received)

        # Write the contributions log
        with gzip.open(res.get_pathname(create=True), "wt") as fd:
            json.dump(parsed, fd)

    res.save()
    log.submission = res
    log.save()
    return res, log


class Command(BaseCommand):
    help = 'Import a data source submission'

    def add_arguments(self, parser):
        parser.add_argument(
                "--quiet", action="store_true", dest="quiet", default=None,
                help="Disable progress reporting")
        parser.add_argument(
                "--method", action="store", default="replace",
                help="Import method (replace or extend)")
        parser.add_argument(
                "source_name", help="Source name")
        parser.add_argument(
                "files", nargs="*", help="Files to load (default: stdin)")

    def handle(self, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        try:
            source = bmodels.Source.objects.get(name=opts["source_name"])
        except bmodels.Source.DoesNotExist:
            raise CommandError(f"Source {opts['source_name']} does not exist")

        # Create submission information directory if it is missing
        source.get_submission_log_dir(create=True)

        i = importer.Importer()
        if opts['files']:
            for fname in opts['files']:
                submission, log = import_file(source, opts["method"], fname)
                log = i.import_submission(submission)
                print(log.to_response().content.decode())
        else:
            with tempfile.NamedTemporaryFile("wt", suffix=".json") as fd:
                shutil.copyfileobj(sys.stdin, fd)
                submission, log = import_file(source, opts["method"], fd.name)
                log = i.import_submission(submission)
                print(log.to_response().content.decode())
