# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand, CommandError
from django.db import models
from contributors import models as cmodels
from contributors import importer
import optparse
import sys
import logging
import json

log = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Export data source information'
    option_list = BaseCommand.option_list + (
        optparse.make_option("--quiet", action="store_true", dest="quiet",
                             default=False, help="Disable progress reporting"),
        optparse.make_option("--with-tokens", action="store_true", dest="with_tokens",
                             default=False, help="Include real auth tokens in the output"),
    )

    def handle(self, quiet=False, with_tokens=False, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if quiet:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        res = cmodels.Source.export_json(args, with_tokens)
        json.dump(res, sys.stdout, indent=2)

