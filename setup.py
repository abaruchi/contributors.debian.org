#!/usr/bin/env python

from distutils.core import setup

setup(
    name='debiancontributors',
    version="0.1",
    description='support functions for contributors.debian.org data sources',
    # long_description='',
    author=['Enrico Zini'],
    author_email=['enrico@debian.org'],
    url='http://contributors.debian.org/',
    install_requires = [],
    license='GPL',
    platforms='any',
    packages=['debiancontributors'],
)
