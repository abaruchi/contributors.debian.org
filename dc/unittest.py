import contributors.models as cmodels
from django.test import Client
from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from rest_framework.test import APIClient
import dc.lib.unittest
import datetime
import re


class NamedObjects(dc.lib.unittest.NamedObjects):
    def create(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        self[_name] = o = self._model.objects.create(**kw)
        return o


class TestUsers(NamedObjects):
    def __init__(self, **defaults):
        defaults.setdefault("full_name", "{_name}")
        defaults.setdefault("username", "{_name}")
        super(TestUsers, self).__init__(cmodels.User, **defaults)

    def create_user(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        self[_name] = o = self._model.objects.create_user(**kw)
        return o

    def create_superuser(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        self[_name] = o = self._model.objects.create_superuser(**kw)
        return o


class TestAuthenticationBackend(ModelBackend):
    pass


class TestBase(dc.lib.unittest.TestBase):
    TEST_AUTH_BACKEND = "dc.unittest.TestAuthenticationBackend"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        if cls.TEST_AUTH_BACKEND not in settings.AUTHENTICATION_BACKENDS:
            settings.AUTHENTICATION_BACKENDS.append(cls.TEST_AUTH_BACKEND)

    @classmethod
    def tearDownClass(cls):
        if cls.TEST_AUTH_BACKEND in settings.AUTHENTICATION_BACKENDS:
            settings.AUTHENTICATION_BACKENDS.remove(cls.TEST_AUTH_BACKEND)
        super().tearDownClass()

    def make_test_client(self, user, signon_identities=()):
        """
        Instantiate a test client, logging in the given person.

        If person is None, visit anonymously. If person is None but
        sso_username is not None, authenticate as the given sso_username even
        if a Person record does not exist.
        """
        client = Client()
        for identity in signon_identities:
            identity = self.identities[identity]
            if identity.issuer == "salsa":
                session = client.session
                session["signon_identity_salsa"] = identity.pk
                session.save()
            elif identity.issuer == "debsso":
                client.defaults["SSL_CLIENT_S_DN_CN"] = identity.subject
            else:
                raise NotImplementedError(f"{identity.issuer} not supported as identity during testing")

        if isinstance(user, str):
            user = self.users[user]
        if user is not None:
            client.force_login(user, backend=self.TEST_AUTH_BACKEND)
        client.visitor = user
        return client

    def make_test_apiclient(self, user, signon_identities=()):
        """
        Instantiate a test client, logging in the given person.

        If person is None, visit anonymously. If person is None but
        sso_username is not None, authenticate as the given sso_username even
        if a Person record does not exist.
        """
        client = APIClient()
        for identity in signon_identities:
            identity = self.identities[identity]
            if identity.issuer == "salsa":
                session = client.session
                session["signon_identity_salsa"] = identity.pk
                session.save()
            elif identity.issuer == "debsso":
                client.defaults["SSL_CLIENT_S_DN_CN"] = identity.subject
            else:
                raise NotImplementedError(f"{identity.issuer} not supported as identity during testing")

        if isinstance(user, str):
            user = self.users[user]
        if user is not None:
            client.force_login(user, backend=self.TEST_AUTH_BACKEND)
        client.visitor = user
        return client

    def assertPermissionDenied(self, response):
        if response.status_code == 403:
            pass
        else:
            self.fail("response has status code {} instead of a 403 Forbidden".format(response.status_code))

    def assertContainsElements(self, response, elements, *names):
        """
        Check that the response contains only the elements in `names` from PageElements `elements`
        """
        want = set(names)
        extras = want - set(elements.keys())
        if extras:
            raise RuntimeError("Wanted elements not found in the list of possible ones: {}".format(", ".join(extras)))
        should_have = []
        should_not_have = []
        content = response.content.decode("utf-8")
        for name, regex in elements.items():
            if name in want:
                if not regex.search(content):
                    should_have.append(name)
            else:
                if regex.search(content):
                    should_not_have.append(name)
        if should_have or should_not_have:
            msg = []
            if should_have:
                msg.append("should have element(s) {}".format(", ".join(should_have)))
            if should_not_have:
                msg.append("should not have element(s) {}".format(", ".join(should_not_have)))
            self.fail("page " + " and ".join(msg))


class BaseFixtureMixin(TestBase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.add_named_objects(
            users=TestUsers(),
            sources=NamedObjects(cmodels.Source),
            ctypes=NamedObjects(cmodels.ContributionType),
            idents=NamedObjects(cmodels.Identifier),
            contributions=NamedObjects(cmodels.Contribution),
        )


class UserFixtureMixin(BaseFixtureMixin):
    """
    Pre-create some users
    """
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.users.create_superuser("admin", username="admin@debian", is_dd=True)
        cls.users.create_user("dd", username="dd@debian", is_dd=True)
        cls.users.create_user("dd1", username="dd1@debian", is_dd=True)
        cls.users.create_user("alioth", username="alioth-guest@alioth")
        cls.users.create_user("alioth1", username="alioth1-guest@alioth")


class SourceFixtureMixin(UserFixtureMixin):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.sources.create(
                "test", name="test", desc="test source", url="http://www.example.org", auth_token="testsecret")
        cls.ctypes.create(
                "tester", source=cls.sources.test, name="tester", desc="tester_desc", contrib_desc="tester_cdesc")

        dates = {
            "begin": datetime.date(2014, 2, 1),
            "until": datetime.date(2014, 2, 15),
        }
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            # Build two IDs per user, one matching the user and one
            # NAME@example.org
            user = cls.users[u]

            name, provider = user.username.split("@", 1)
            if provider == "debian":
                email = f"{name}@debian.org"
            elif provider == "alioth":
                email = f"{name}@users.alioth.debian.org"
            elif provider == "salsa":
                email = f"{name}@salsa.debian.org"

            id1 = "{}_user".format(u)
            cls.idents.create(id1, type="email", name=email, user=user)

            id2 = "{}_home".format(u)
            home_email = re.sub(r"(?:-guest)?@.+", "@example.org", user.username)
            cls.idents.create(id2, type="email", name=home_email, user=user)

            # Make a contribution for each identifier
            cls.contributions.create(id1, type=cls.ctypes.tester, identifier=cls.idents[id1], **dates)
            cls.contributions.create(id2, type=cls.ctypes.tester, identifier=cls.idents[id2], **dates)


class PageElements(dict):
    """
    List of all page elements possibly expected in the results of a view.

    dict matching name used to refer to the element with regexp matching the
    element.
    """
    def add_id(self, id):
        self[id] = re.compile(r"""id\s*=\s*["']{}["']""".format(re.escape(id)))

    def add_class(self, cls):
        self[cls] = re.compile(r"""class\s*=\s*["']{}["']""".format(re.escape(cls)))

    def add_href(self, name, url):
        self[name] = re.compile(r"""href\s*=\s*["']{}["']""".format(re.escape(url)))

    def add_string(self, name, term):
        self[name] = re.compile(r"""{}""".format(re.escape(term)))

    def clone(self):
        res = PageElements()
        res.update(self.items())
        return res


class SignonFixtureMixin(BaseFixtureMixin):
    """
    Base test fixture for signon tests
    """
    @classmethod
    def setUpClass(cls):
        from signon.models import Identity
        super().setUpClass()
        cls.add_named_objects(
            identities=NamedObjects(Identity),
        )
        cls.users.create_user("user1", username="user1@debian")
        cls.users.create_user("user2", username="user2@debian")

        cls.user1 = cls.users.user1
        cls.user2 = cls.users.user2


class ImpersonateFixtureMixin(BaseFixtureMixin):
    """
    Base test fixture for impersonate tests
    """
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.users.create_superuser("admin", username="admin@debian")
        cls.users.create_user("user1", username="user1@debian")
        cls.users.create_user("user2", username="user2@debian")

        cls.admin = cls.users.admin
        cls.user1 = cls.users.user1
        cls.user2 = cls.users.user2
