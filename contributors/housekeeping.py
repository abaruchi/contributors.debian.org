from __future__ import annotations
import django_housekeeping as hk
from django.conf import settings
from contributors.models import User
import requests
import logging
import os
import json
from . import models as cmodels

log = logging.getLogger(__name__)

STAGES = ["backup", "harvest", "main", "association", "usermerging", "aggregation", "stats"]

DATA_DIR = getattr(settings, "DATA_DIR", None)

UID_ALIOTH_DOMAIN = "@users.alioth.debian.org"
UID_DEBIAN_DOMAIN = "@debian.org"


class BackupAssociations(hk.Task):
    """
    Backup user/identifier associations
    """
    def run_backup(self, stage):
        if not self.hk.outdir:
            return
        outfile = os.path.join(self.hk.outdir.path("backup"), "associations.json")

        res = []
        for ident in cmodels.Identifier.objects.filter(user__isnull=False):
            res.append({
                "type": ident.type,
                "name": ident.name,
                "user": ident.user.username,
            })

        with open(outfile, "wt") as out:
            json.dump(res, out, indent=1)


class BackupUsers(hk.Task):
    """
    Backup User info
    """
    def run_backup(self, stage):
        if not self.hk.outdir:
            return
        outfile = os.path.join(self.hk.outdir.path("backup"), "users.json")

        res = []
        for user in cmodels.User.objects.all():
            res.append({
                "username": user.username,
                "full_name": user.full_name,
                "is_dd": user.is_dd,
                "is_staff": user.is_staff,
                "hidden": user.hidden,
            })

        with open(outfile, "wt") as out:
            json.dump(res, out, indent=1)


class BackupIdentifiers(hk.Task):
    """
    Backup Identifiers info
    """
    def run_backup(self, stage):
        if not self.hk.outdir:
            return
        outfile = os.path.join(self.hk.outdir.path("backup"), "identifiers.json")

        res = []
        for ident in cmodels.Identifier.objects.all():
            res.append({
                "type": ident.type,
                "name": ident.name,
                "hidden": ident.hidden,
            })

        with open(outfile, "wt") as out:
            json.dump(res, out, indent=1)


class CleanupDataDir(hk.Task):
    """
    Expire (delete) old files from recent history data dir
    """
    def run_backup(self, stage):
        from sources.utils import list_backup_files, choose_files_to_remove
        dry_run = self.hk.dry_run

        data_dir = getattr(settings, "DATA_DIR", None)
        if data_dir is None:
            return

        backupdir = os.path.join(data_dir, "contrib-backups")
        logdir = os.path.join(data_dir, "submissions")

        for path in backupdir, logdir:
            for pathname in choose_files_to_remove(list_backup_files(path)):
                if not dry_run:
                    os.unlink(pathname)
                log.info("%s: removed %s", self.IDENTIFIER, pathname)


class AggregateContributors(hk.Task):
    """
    Recompute the Aggregated tables
    """
    def run_aggregation(self, stage):
        cmodels.AggregatedPersonContribution.recompute()
        cmodels.AggregatedSource.recompute()
        cmodels.AggregatedPerson.recompute()


class FixFullNames(hk.Task):
    """
    Clean User.full_name fields that contain only spaces, setting them to the
    empty string
    """
    def run_main(self, stage):
        dry_run = self.hk.dry_run

        for u in User.objects.all():
            if u.full_name.isspace():
                u.add_log("full name for {} was only made of spaces: setting it to the empty string".format(u))
                u.full_name = ""
                if not dry_run:
                    u.save()


class ImportNMIdentities(hk.Task):
    """
    Aggregate from the nm.debian.org export_identities API
    """

    def fetch_export_identities(self):
        api_key = getattr(settings, "NM_API_KEY", None)
        if api_key is None:
            return None

        # See https://wiki.debian.org/ServicesSSL#python-requests
        bundle = '/etc/ssl/ca-debian/ca-certificates.crt'
        if os.path.exists(bundle):
            r = requests.get("https://nm.debian.org/api/export_identities",
                             headers={"Api-Key": api_key}, verify=bundle)
        else:
            r = requests.get("https://nm.debian.org/api/export_identities", headers={"Api-Key": api_key})
        return r.json()

    def run_main(self, stage):
        from signon.models import Identity

        dry_run = self.hk.dry_run

        # Load local identities, indexed by (issue, subject)
        identities = {}
        for identity in Identity.objects.select_related("person"):
            identities[(identity.issuer, identity.subject)] = identity

        people = self.fetch_export_identities()
        # People with inconsistent mappings, that we won't touch
        skip_users = set()
        # Remote identities indexed by local users
        remote_persons = {}
        for remote_person in people:
            # Match remote and local user by their identitites
            local_person = None
            for remote_identity in remote_person["identities"]:
                local_identity = identities.get((remote_identity["issuer"], remote_identity["subject"]))
                if local_identity is None:
                    continue
                if local_person is None:
                    local_person = local_identity.person
                elif local_person != local_identity.person:
                    # Warn if we find two local identitites for one remote person
                    log.error(
                        "%s: remote person %s <%s> maps to multiple local people %s and %s",
                        self.IDENTIFIER, remote_person["full_name"], remote_person["ldap_uid"],
                        local_person, local_identity.person)
                    skip_users.add(local_person)
                    skip_users.add(local_identity.person)

            if local_person is None:
                # Do not warn of remote users not present locally, as it's
                # possible that many people known by nm.d.o have not logged
                # into here
                # log.info(
                #     "%s: remote person %s <%s> does not map to any local user",
                #     self.IDENTIFIER, remote_person["full_name"], remote_person["ldap_uid"])
                continue

            old = remote_persons.get(local_person)
            if old is not None:
                log.error(
                    "%s: local person %s maps to multiple remote people %s <%s> and %s <%s>",
                    self.IDENTIFIER, local_person, old["full_name"], old["ldap_uid"],
                    remote_person["full_name"], remote_person["ldap_uid"])
                skip_users.add(local_person)
            else:
                remote_persons[local_person] = remote_person

        for local_user in cmodels.User.objects.all():
            changes = []

            remote_person = remote_persons.get(local_user)
            if local_user in skip_users or remote_person is None:
                # TODO: clear
                is_dd = False
            else:
                # set is_dd
                is_dd = remote_person["status"] == "debian_developer"

                # set username as ldap_uid if present
                if remote_person["ldap_uid"] and remote_person["ldap_uid"] != local_user.username:
                    changes.append(
                            f"set username of {local_user} to {remote_person['ldap_uid']} from nm.debian.org information")
                    local_user.username = remote_person["ldap_uid"]

                # TODO: auto-associate 'login' identifiers with 'debsso @debian.org' identities

            if local_user.is_dd != is_dd:
                changes.append(f"set is_dd of {local_user} to {is_dd} from nm.debian.org information")
                local_user.is_dd = is_dd

            for change in changes:
                log.info("%s: %s", self.IDENTIFIER, change)
                if not dry_run:
                    local_user.add_log(change)

            if changes:
                if not dry_run:
                    local_user.save()
