# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('contributors', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='source',
            name='name',
            field=models.CharField(help_text='Data source name, shown on the website and used as a data source identifier when submitting contribution data.', unique=True, max_length=32, validators=[django.core.validators.RegexValidator('^[A-Za-z0-9._ -]+$', 'Please use only letters, numbers, spaces, dots, dashes or underscores')]),
            preserve_default=True,
        ),
    ]
