from __future__ import annotations
from django.test import TestCase
from dc.unittest import SourceFixtureMixin
import contributors.models as cmodels
import datetime


class APCTestCase(SourceFixtureMixin, TestCase):
    def test_aggregate_all(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute()

        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 5)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.admin).order_by("ctype__name"))
        self.assertEqual(len(c), 1)

        self.assertEqual(c[0].user, self.users.admin)
        self.assertEqual(c[0].ctype, self.ctypes.tester)
        self.assertEqual(c[0].begin, datetime.date(2014, 2, 1))
        self.assertEqual(c[0].until, datetime.date(2014, 2, 15))

    def test_aggregate_user(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute(user=self.users.admin)

        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 1)

        c = cmodels.AggregatedPersonContribution.objects.get(user=self.users.admin)
        self.assertEqual(c.user, self.users.admin)
        self.assertEqual(c.ctype, self.ctypes.tester)
        self.assertEqual(c.begin, datetime.date(2014, 2, 1))
        self.assertEqual(c.until, datetime.date(2014, 2, 15))

    def test_aggregate_ctype(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute(ctype=self.ctypes.tester)

        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 5)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.admin).order_by("ctype__name"))
        self.assertEqual(len(c), 1)

        self.assertEqual(c[0].user, self.users.admin)
        self.assertEqual(c[0].ctype, self.ctypes.tester)
        self.assertEqual(c[0].begin, datetime.date(2014, 2, 1))
        self.assertEqual(c[0].until, datetime.date(2014, 2, 15))

    def test_aggregate_user_ctype(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute(user=self.users.admin, ctype=self.ctypes.tester)

        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 1)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.admin).order_by("ctype__name"))
        self.assertEqual(len(c), 1)

        self.assertEqual(c[0].user, self.users.admin)
        self.assertEqual(c[0].ctype, self.ctypes.tester)
        self.assertEqual(c[0].begin, datetime.date(2014, 2, 1))
        self.assertEqual(c[0].until, datetime.date(2014, 2, 15))

    def test_aggregate_partial(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Compute all
        cmodels.AggregatedPersonContribution.recompute()

        # Tweak a contribution
        c = cmodels.Contribution.objects.get(type=self.ctypes.tester, identifier=self.idents.admin_home)
        c.begin = datetime.date(2014, 3, 17)
        c.until = datetime.date(2014, 3, 19)
        c.save()

        # Reaggregate only a user/ctype
        cmodels.AggregatedPersonContribution.recompute(user=self.users.admin, ctype=self.ctypes.tester)

        # Still there are all the other entries
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 5)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.admin).order_by("ctype__name"))
        self.assertEqual(len(c), 1)

        self.assertEqual(c[0].user, self.users.admin)
        self.assertEqual(c[0].ctype, self.ctypes.tester)
        self.assertEqual(c[0].begin, datetime.date(2014, 2, 1))
        self.assertEqual(c[0].until, datetime.date(2014, 3, 19))

    def test_hide_user(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        self.users.dd.hidden = True
        self.users.dd.save()

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute()
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 4)

        # There are no entries for identifiers of that user
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.dd).exists())

        # The rest is still ok
        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.admin).order_by("ctype__name"))
        self.assertEqual(len(c), 1)

        self.assertEqual(c[0].user, self.users.admin)
        self.assertEqual(c[0].ctype, self.ctypes.tester)
        self.assertEqual(c[0].begin, datetime.date(2014, 2, 1))
        self.assertEqual(c[0].until, datetime.date(2014, 2, 15))

    def test_hide_ident(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        self.idents.admin_home.hidden = True
        self.idents.admin_home.save()

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute()
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 5)

        # There are entries for that user, limited to the visible identifiers
        self.assertTrue(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.admin).exists())

        # The rest is still ok
        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.admin).order_by("ctype__name"))
        self.assertEqual(len(c), 1)

        self.assertEqual(c[0].user, self.users.admin)
        self.assertEqual(c[0].ctype, self.ctypes.tester)
        self.assertEqual(c[0].begin, datetime.date(2014, 2, 1))
        self.assertEqual(c[0].until, datetime.date(2014, 2, 15))

    def test_hide_source(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        s = self.sources.test.get_settings(self.users.dd, create=True)
        s.hidden = True
        s.save()

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute()
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 4)

        # There are entries for that user, limited to the visible identifiers
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.dd).exists())

        # The rest is still ok
        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.users.admin).order_by("ctype__name"))
        self.assertEqual(len(c), 1)

        self.assertEqual(c[0].user, self.users.admin)
        self.assertEqual(c[0].ctype, self.ctypes.tester)
        self.assertEqual(c[0].begin, datetime.date(2014, 2, 1))
        self.assertEqual(c[0].until, datetime.date(2014, 2, 15))


class APTestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.ctypes.create(
                "pinger", source=cls.sources.test, name="pinger", desc="Pinger", contrib_desc="Pinging")

    def test_simple(self):
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.users.admin,
            ctype=self.ctypes.tester,
            begin=datetime.date(2014, 1, 1),
            until=datetime.date(2014, 2, 1),
        )

        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 0)

        cmodels.AggregatedPerson.recompute()

        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 1)
        ap = cmodels.AggregatedPerson.objects.get()
        self.assertEqual(ap.user, self.users.admin)
        self.assertEqual(ap.begin, datetime.date(2014, 1, 1))
        self.assertEqual(ap.until, datetime.date(2014, 2, 1))

    def test_two_sources(self):
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.users.admin,
            ctype=self.ctypes.tester,
            begin=datetime.date(2014, 1, 1),
            until=datetime.date(2014, 2, 1),
        )
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.users.admin,
            ctype=self.ctypes.pinger,
            begin=datetime.date(2014, 3, 1),
            until=datetime.date(2014, 4, 1),
        )

        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 0)

        cmodels.AggregatedPerson.recompute()

        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 1)
        ap = cmodels.AggregatedPerson.objects.get()
        self.assertEqual(ap.user, self.users.admin)
        self.assertEqual(ap.begin, datetime.date(2014, 1, 1))
        self.assertEqual(ap.until, datetime.date(2014, 4, 1))

    def test_recompute(self):
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.users.admin,
            ctype=self.ctypes.tester,
            begin=datetime.date(2014, 1, 1),
            until=datetime.date(2014, 2, 1),
        )
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.users.dd,
            ctype=self.ctypes.tester,
            begin=datetime.date(2014, 1, 1),
            until=datetime.date(2014, 2, 1),
        )

        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 0)

        # Recompute all
        cmodels.AggregatedPerson.recompute()

        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 2)
        aps = list(cmodels.AggregatedPerson.objects.order_by("user__username"))
        self.assertEqual(aps[0].user, self.users.admin)
        self.assertEqual(aps[0].begin, datetime.date(2014, 1, 1))
        self.assertEqual(aps[0].until, datetime.date(2014, 2, 1))
        self.assertEqual(aps[1].user, self.users.dd)
        self.assertEqual(aps[1].begin, datetime.date(2014, 1, 1))
        self.assertEqual(aps[1].until, datetime.date(2014, 2, 1))

        # Add a new entry
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.users.admin,
            ctype=self.ctypes.pinger,
            begin=datetime.date(2014, 3, 1),
            until=datetime.date(2014, 4, 1),
        )

        # Partial recompute
        cmodels.AggregatedPerson.recompute(user=self.users.admin)

        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 2)
        aps = list(cmodels.AggregatedPerson.objects.order_by("user__username"))
        self.assertEqual(aps[0].user, self.users.admin)
        self.assertEqual(aps[0].begin, datetime.date(2014, 1, 1))
        self.assertEqual(aps[0].until, datetime.date(2014, 4, 1))
        self.assertEqual(aps[1].user, self.users.dd)
        self.assertEqual(aps[1].begin, datetime.date(2014, 1, 1))
        self.assertEqual(aps[1].until, datetime.date(2014, 2, 1))

        # Add a new entry
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.users.dd,
            ctype=self.ctypes.pinger,
            begin=datetime.date(2014, 3, 1),
            until=datetime.date(2014, 3, 15),
        )

        # Total recompute
        cmodels.AggregatedPerson.recompute()

        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 2)
        aps = list(cmodels.AggregatedPerson.objects.order_by("user__username"))
        self.assertEqual(aps[0].user, self.users.admin)
        self.assertEqual(aps[0].begin, datetime.date(2014, 1, 1))
        self.assertEqual(aps[0].until, datetime.date(2014, 4, 1))
        self.assertEqual(aps[1].user, self.users.dd)
        self.assertEqual(aps[1].begin, datetime.date(2014, 1, 1))
        self.assertEqual(aps[1].until, datetime.date(2014, 3, 15))
