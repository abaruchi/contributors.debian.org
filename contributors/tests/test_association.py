from __future__ import annotations
from django_housekeeping import Housekeeping
from django.test import TestCase
from dc.unittest import NamedObjects, UserFixtureMixin
from signon.models import Identity
from unittest import mock
import re


class HousekeepingTest:
    def run_housekeeping(self, match=""):
        # Run AssociateLDAPEmails
        hk = Housekeeping(dry_run=False)
        hk.autodiscover()
        hk.init()
        with self.assertLogs(level="INFO") as log:
            hk.run(run_filter=lambda name: re.search(match, name))

        is_ok = True
        for entry in log.output:
            if entry.startswith("ERROR:"):
                is_ok = True

        if not is_ok:
            self.fail("housekeeping had errors")

        return log


class TestNMSync(HousekeepingTest, UserFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.add_named_objects(
            identities=NamedObjects(Identity),
        )
        cls.identities.create(
                "alioth_salsa", person=cls.users.alioth,
                issuer="salsa", subject="2", username="alioth", audit_skip=True)
        cls.identities.create(
                "alioth_debssso", person=cls.users.alioth,
                issuer="debsso", subject="alioth@users.alioth.debian.org",
                username="alioth@users.alioth.debian.org", audit_skip=True)
        cls.identities.create(
                "alioth1_salsa", person=cls.users.alioth1,
                issuer="salsa", subject="3", username="alioth1", audit_skip=True)
        cls.identities.create(
                "alioth1_debssso", person=cls.users.alioth1,
                issuer="debsso", subject="alioth1@users.alioth.debian.org",
                username="alioth1@users.alioth.debian.org", audit_skip=True)

    def run_housekeeping(self, mock_data=[]):
        with mock.patch("contributors.housekeeping.ImportNMIdentities.fetch_export_identities", return_value=mock_data):
            log = super().run_housekeeping(match="ImportNMIdentities")
            return [x for x in log.output if not x.startswith("INFO:django_housekeeping.run:")]

    def testIsDD(self):
        self.maxDiff = None

        nm_data = [
            {
             "full_name": "Alioth",
             "email": "alioth@debian.org",
             "status": "debian_developer",
             "ldap_uid": None,
             "identities": [
              {
               "issuer": "salsa",
               "subject": "2",
               "username": "alioth-salsa",
              }
             ]
            },
        ]
        log_output = self.run_housekeeping(nm_data)

        HEAD = "INFO:contributors.housekeeping:contributors.ImportNMIdentities"
        self.assertEqual(log_output, [
            f'{HEAD}: set is_dd of admin@debian to False from nm.debian.org information',
            f'{HEAD}: set is_dd of dd@debian to False from nm.debian.org information',
            f'{HEAD}: set is_dd of dd1@debian to False from nm.debian.org information',
            f"{HEAD}: set is_dd of alioth-guest@alioth to True from nm.debian.org information",
        ])

        self.assertEqual([x.text for x in self.users.alioth.log.all()], [
            'set is_dd of alioth-guest@alioth to True from nm.debian.org information',
        ])

        self.users.alioth.refresh_from_db()

        self.assertTrue(self.users.alioth.is_dd)
        self.assertEqual(self.users.alioth.username, "alioth-guest@alioth")

    def testUsername(self):
        self.maxDiff = None

        nm_data = [
            {
             "full_name": "Alioth",
             "email": "alioth@debian.org",
             "status": "debian_maintainer",
             "ldap_uid": "alioth",
             "identities": [
              {
               "issuer": "salsa",
               "subject": "2",
               "username": "alioth-salsa",
              }
             ]
            },
        ]
        log_output = self.run_housekeeping(nm_data)

        HEAD = "INFO:contributors.housekeeping:contributors.ImportNMIdentities"
        self.assertEqual(log_output, [
            f'{HEAD}: set is_dd of admin@debian to False from nm.debian.org information',
            f'{HEAD}: set is_dd of dd@debian to False from nm.debian.org information',
            f'{HEAD}: set is_dd of dd1@debian to False from nm.debian.org information',
            f"{HEAD}: set username of alioth-guest@alioth to alioth from nm.debian.org information",
        ])

        self.assertEqual([x.text for x in self.users.alioth.log.all()], [
            "set username of alioth-guest@alioth to alioth from nm.debian.org information",
        ])

        self.users.alioth.refresh_from_db()

        self.assertFalse(self.users.alioth.is_dd)
        self.assertEqual(self.users.alioth.username, "alioth")

    def testOneLocalMapsToTwoRemotes(self):
        self.maxDiff = None

        nm_data = [
            {
             "full_name": "Alioth",
             "email": "alioth@users.alioth.debian.org.org",
             "status": "debian_maintainer",
             "ldap_uid": "alioth",
             "identities": [
              {
               "issuer": "salsa",
               "subject": "2",
               "username": "alioth-salsa",
              }
             ]
            },
            {
             "full_name": "Alioth1",
             "email": "alioth@debian.org",
             "status": "debian_developer",
             "ldap_uid": "alioth1",
             "identities": [
              {
               "issuer": "debsso",
               "subject": "alioth@users.alioth.debian.org",
               "username": "alioth-debsso",
              }
             ]
            },
        ]
        log_output = self.run_housekeeping(nm_data)

        HEAD = "contributors.housekeeping:contributors.ImportNMIdentities"
        self.assertEqual(log_output, [
            f"ERROR:{HEAD}: "
            "local person alioth-guest@alioth maps to multiple remote people Alioth <alioth> and Alioth1 <alioth1>",
            f'INFO:{HEAD}: set is_dd of admin@debian to False from nm.debian.org information',
            f'INFO:{HEAD}: set is_dd of dd@debian to False from nm.debian.org information',
            f'INFO:{HEAD}: set is_dd of dd1@debian to False from nm.debian.org information',
        ])

        self.assertEqual([x.text for x in self.users.alioth.log.all()], [
        ])

        self.users.alioth.refresh_from_db()

        self.assertFalse(self.users.alioth.is_dd)
        self.assertEqual(self.users.alioth.username, "alioth-guest@alioth")

    def testOneRemoteMapsToTwoLocal(self):
        self.maxDiff = None

        nm_data = [
            {
             "full_name": "Alioth",
             "email": "alioth@users.alioth.debian.org.org",
             "status": "debian_maintainer",
             "ldap_uid": "alioth",
             "identities": [
              {
               "issuer": "salsa",
               "subject": "2",
               "username": "alioth-salsa",
              }, {
               "issuer": "debsso",
               "subject": "alioth1@users.alioth.debian.org",
               "username": "alioth1-debsso",
              }
             ]
            },
        ]
        log_output = self.run_housekeeping(nm_data)

        HEAD = "contributors.housekeeping:contributors.ImportNMIdentities"
        self.assertEqual(log_output, [
            f"ERROR:{HEAD}: "
            "remote person Alioth <alioth> maps to multiple local people alioth-guest@alioth and alioth1-guest@alioth",
            f'INFO:{HEAD}: set is_dd of admin@debian to False from nm.debian.org information',
            f'INFO:{HEAD}: set is_dd of dd@debian to False from nm.debian.org information',
            f'INFO:{HEAD}: set is_dd of dd1@debian to False from nm.debian.org information',
        ])

        self.assertEqual([x.text for x in self.users.alioth.log.all()], [])

        self.users.alioth.refresh_from_db()

        self.assertFalse(self.users.alioth.is_dd)
        self.assertEqual(self.users.alioth.username, "alioth-guest@alioth")

    def testConsistentInfo(self):
        self.maxDiff = None

        nm_data = [
            {
             "full_name": "Alioth",
             "email": "alioth@users.alioth.debian.org.org",
             "status": "debian_developer",
             "ldap_uid": "alioth",
             "identities": [
              {
               "issuer": "salsa",
               "subject": "2",
               "username": "alioth-salsa",
              }, {
               "issuer": "debsso",
               "subject": "alioth@users.alioth.debian.org",
               "username": "alioth-debsso",
              }
             ]
            },
        ]
        log_output = self.run_housekeeping(nm_data)

        HEAD = "INFO:contributors.housekeeping:contributors.ImportNMIdentities"
        self.assertEqual(log_output, [
            f'{HEAD}: set is_dd of admin@debian to False from nm.debian.org information',
            f'{HEAD}: set is_dd of dd@debian to False from nm.debian.org information',
            f'{HEAD}: set is_dd of dd1@debian to False from nm.debian.org information',
            f"{HEAD}: set username of alioth-guest@alioth to alioth from nm.debian.org information",
            f"{HEAD}: set is_dd of alioth to True from nm.debian.org information",
        ])

        self.assertEqual([x.text for x in self.users.alioth.log.all()], [
            "set username of alioth-guest@alioth to alioth from nm.debian.org information",
            "set is_dd of alioth to True from nm.debian.org information",
        ])

        self.users.alioth.refresh_from_db()

        self.assertTrue(self.users.alioth.is_dd)
        self.assertEqual(self.users.alioth.username, "alioth")
