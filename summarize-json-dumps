#!/usr/bin/python3

import argparse
import sys
import logging
import json
import os
import datetime
import re
import gzip
from contextlib import contextmanager

log = logging.getLogger("summarize")


class Fail(Exception):
    pass


class Summary:
    """
    Maintain a summary of all changes to the nm.debian.org database
    """
    def __init__(self):
        # List of (timestamp, person) indexed by lookup key
        self.users = {}

    @contextmanager
    def open(self, fn, mode):
        if fn.endswith(".gz"):
            with gzip.open(fn, mode) as fd:
                yield fd
        else:
            with open(fn, mode) as fd:
                yield fd

    def load(self, fn):
        try:
            with self.open(fn, "rt") as fd:
                self.users = json.load(fd)
        except FileNotFoundError:
            pass

    def save(self, fn):
        with self.open(fn, "wt") as fd:
            json.dump(self.users, fd)

    def add_dir(self, dirname):
        """
        Find all yyyymmdd/{associations,identifiers,users}.json and add them to the summary
        """
        re_dir_date = re.compile(r".+/(\d{8}(?:-\d{6})?)\W")

        # Find paths that contain files to summarize
        paths = []
        for (dirpath, dirnames, filenames, dirfd) in os.fwalk(dirname):
            if os.path.basename(dirpath) != "backup":
                continue

            misses_files = False
            for fn in "associations.json", "identifiers.json", "users.json":
                if fn not in filenames:
                    log.error("%s: skipping path with no %s", dirpath, fn)
                    misses_files = True
            if misses_files:
                continue

            mo = re_dir_date.match(dirpath)
            if not mo:
                log.error("%s: skipping path with unrecogniseable date", dirpath)
                continue

            if len(mo.group(1)) == 8:
                dt = datetime.datetime.strptime(mo.group(1), "%Y%m%d")
            else:
                dt = datetime.datetime.strptime(mo.group(1), "%Y%m%d-%H%M%S")

            log.debug("Found %s", dt)

            dt = dt.strftime("%Y%m%d%H%M%S")
            paths.append((dt, dirpath))

        # Sort them by date
        paths.sort()

        for dt, dirpath in paths:
            with open(os.path.join(dirpath, "associations.json"), "rt") as fd:
                associations = json.load(fd)
            with open(os.path.join(dirpath, "identifiers.json"), "rt") as fd:
                identifiers = json.load(fd)
            with open(os.path.join(dirpath, "users.json"), "rt") as fd:
                users = json.load(fd)

            self.add_dump(dirpath, dt, associations, identifiers, users)

    def add_dump(self, fn, dt, associations, identifiers, users):
        by_email = {}

        # Load identifier information
        hidden_identifiers = set()
        for ident in identifiers:
            if ident["hidden"]:
                hidden_identifiers.add((ident["type"], ident["name"]))

        # Load user information
        for u in users:
            # Compatibility with old model
            if "username" not in u:
                u["username"] = u["email"]
            name = u.get("username")
            if name is None:
                log.error("%s: no username or email found for %r", fn, u)
                raise NotImplementedError("input not supported")
            by_email[name] = u
            u["idents"] = []

        # Load association information
        for a in associations:
            by_email[a["user"]]["idents"].append({
                "type": a["type"],
                "name": a["name"],
                "hidden": (a["type"], a["name"]) in hidden_identifiers,
            })

        new = 0
        updated = 0
        unchanged = 0

        for username, user in by_email.items():
            history = self.users.get(username)
            if history is None:
                new += 1
                self.users[username] = [(dt, user)]
            elif history[-1][0] > dt:
                log.warn(
                    "%s: last seen for %s is from %s but found earlier %s: skipped",
                    fn, username, history[-1][0], dt)
            elif history[-1][1] != user:
                updated += 1
                history.append((dt, user))
            else:
                unchanged += 1

        removed = 0
        for username in self.users.keys() - by_email.keys():
            history = self.users.get(username)
            if history[-1][1] is not None:
                history.append((dt, None))
                removed += 1

        log.info("%s: %d new, %d updated, %d unchanged %d removed", fn, new, updated, unchanged, removed)

    def print_history(self, email):
        try:
            from jsondiff import JsonDiffer
            differ = JsonDiffer(syntax="explicit")
        except ModuleNotFoundError:
            differ = None

        person = self.users.get(email)
        if person is None:
            log.error("%s: person not found", email)
            return

        last = None
        for dt, data in person:
            print("# {}".format(dt))
            if last is None or differ is None:
                json.dump(data, sys.stdout, indent=1)
                print()
            else:
                d = differ.diff(last, data)
                json.dump(differ.marshal(d), sys.stdout, indent=1)
                print()
            last = data


def main():
    parser = argparse.ArgumentParser(description="Maintain a summary of all changes to the nm.debian.org database")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")
    parser.add_argument("--debug", action="store_true", help="debug output")
    parser.add_argument("-d", "--db", action="store", help="merged JSON data")
    parser.add_argument("--add-dir", action="store", metavar="dir", help="add all JSON files from the given directory")
    parser.add_argument("--users", action="store_true", help="show all users known to the summary")
    parser.add_argument("--history", action="store", metavar="key", help="show the history for a key")

    args = parser.parse_args()

    log_format = "%(levelname)s %(message)s"
    level = logging.WARN
    if args.debug:
        level = logging.DEBUG
    elif args.verbose:
        level = logging.INFO
    logging.basicConfig(level=level, stream=sys.stderr, format=log_format)

    summary = Summary()
    if args.db:
        summary.load(args.db)

    modified = False

    if args.add_dir:
        modified = True
        summary.add_dir(args.add_dir)

    if args.users:
        for user in sorted(summary.users.keys()):
            print(user)

    if args.history:
        summary.print_history(args.history)

    if modified and args.db:
        summary.save(args.db)


if __name__ == "__main__":
    try:
        main()
    except Fail as e:
        print(e, file=sys.stderr)
        sys.exit(1)
    except Exception:
        log.exception("uncaught exception")
