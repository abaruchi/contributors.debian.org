from __future__ import annotations
from django.test import TestCase
from django.urls import reverse
from impersonate.unittest import ImpersonateFixtureMixin


class TestPermissions(ImpersonateFixtureMixin, TestCase):
    def test_impersonate_staff(self):
        visitor = self.admin
        visited = self.user1
        client = self.make_test_client(visitor)

        response = client.get(reverse("impersonate:whoami"))
        self.assertJSONEqual(response.content, {
            'impersonator': None,
            'impersonator_desc': None,
            'user': visitor.pk,
            'user_desc': str(visitor),
        })

        response = client.post(reverse("impersonate:impersonate"), data={"pk": visited.pk, "next": "/"})
        self.assertRedirectMatches(response, "^/$")

        response = client.get(reverse("impersonate:whoami"))
        self.assertJSONEqual(response.content, {
            'impersonator': visitor.pk,
            'impersonator_desc': str(visitor),
            'user': visited.pk,
            'user_desc': str(visited),
        })

    def test_impersonate_user(self):
        visitor = self.user1
        visited = self.user2
        client = self.make_test_client(visitor)
        response = client.post(reverse("impersonate:impersonate"), data={"pk": visited.pk})
        self.assertPermissionDenied(response)

        response = client.get(reverse("impersonate:whoami"))
        self.assertJSONEqual(response.content, {
            'impersonator': None,
            'impersonator_desc': None,
            'user': visitor.pk,
            'user_desc': str(visitor),
        })

    def test_impersonate_anonymous(self):
        visited = self.user1
        client = self.make_test_client(None)
        response = client.post(reverse("impersonate:impersonate"), data={"pk": visited.pk})
        self.assertPermissionDenied(response)

        response = client.get(reverse("impersonate:whoami"))
        self.assertJSONEqual(response.content, {
            'impersonator': None,
            'impersonator_desc': None,
            'user': None,
            'user_desc': "AnonymousUser",
        })
