from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^last-significant.json$', views.ByFingerprint.as_view(), name="mia_last_significant"),
]

