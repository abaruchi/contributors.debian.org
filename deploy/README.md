# Automatic deploy from gitlab CI

This is a Django app to support automatic deployment using gitlab's CI.

The workflow is this:

 - gitlab runs tests in the CI
 - gitlab notifies pipeline status changes via a webhook
 - when a selected pipeline changes status to `success`, the application queues
   a deploy for that shasum by creating a `shasum.deploy` file in a queue
   directory
 - a systemd `.path` unit running as the deploy user triggers when the new file
   is created and runs `manage.py deploy` as the deploy user

`manage.py deploy` does this:

 - git fetch
 - abort of the shasum of the head of the deploy branch does not match one of
   the `.deploy` files in the queue directory
 - abort if the head of the deploy branch is not signed by a gpg key present in
   a deploy keyring
 - abort if the head of the deploy branch is not a successor of the currently
   deployed commit
 - update the working copy
 - run a deploy script
 - remove all `.deploy` files seen when the script was called
 - send an email to the site admins with a log of the whole deploy process,
   whether it succeeded or it was aborted

## Setting it up

### Configure gitlab webhook

Add a webhook under Settings/Integrations.

Trigger on "Pipeline events".

URL: `https://<site>/deploy/gitlab-pipeline-hook`

Optionally configure a secret token.

### `settings.py` keys

 * `DEPLOY_GITLAB_PIPELINE_NAME`: name of the pipeline that, when succeeded,
   triggers a deploy
 * `DEPLOY_GITLAB_TOKEN`: set to the secret token configured in the gitlab
   webhook. If set, webhook posts without the secret token are refused. If not
   set, no secret token is checked
 * `DEPLOY_KEYS_DIR`: directory which contains the ascii-armored public keys of
   the people who can sign commits to be deployed. Files can have any name, and
   need to end with `.gpg`
 * `DEPLOY_BRANCH`: git branch to use for deployment
 * `DEPLOY_QUEUE_DIR`: directory where `shasum.deploy` files are written. It
   needs to be writable by both the web application and the deploy user

### System configuration

Create the queued directory, writable by both web services and deploy user.

Enable lingering for the deploy user.

Set up systemd units in `~/.config/systemd/user` for the deploy user:

Path activation `deploy.path`:
```
[Unit]
Descriptor=Monitor <path to queue dir> for deploy requests

[Path]
PathExistsGlob=<path to queue dir>/*.deploy

[Install]
WantedBy=default.target
```

Path activated service `deploy.service`:
```
[Unit]
Description=Run deploy script

[Service]
Type=oneshot
ExecStart=<path to working directory>/manage.py deploy

[Install]
WantedBy=default.target
```

Enable the units:

```
systemctl --user enable deploy.path
systemctl --user enable deploy.service
```

If you get this error with `systemctl --user`:
```
$ systemctl --user status
Failed to connect to bus: No such file or directory
```

Run this:

```
export XDG_RUNTIME_DIR=/run/user/$(id -u)
```

(see <https://askubuntu.com/questions/813588/systemctl-failed-to-connect-to-bus-docker-ubuntu16-04-container>)


### Configure gitlab CI

This is an example `.gitlab-ci.yml`:

```
stages:
 - test

tests:
 stage: test
 script:
  - apt-get update
  - apt-get -y install python3 python3-django python3-django-housekeeping python3-debiancontributors python3-djangorestframework python3-model-mommy python3-git
  - python3 manage.py test
```

The corresponding `DEPLOY_GITLAB_PIPELINE_NAME` setting for that CI setup is: `DEPLOY_GITLAB_PIPELINE_NAME=tests`


### Test

 - Push a signed commit to the deploy branch
 - Look at the pipeline to see if the tests pass
 - Receive a mail with the deploy results
